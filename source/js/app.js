$('.menu-icon').on('click', function(){
  $('.top-nav').toggleClass('active');
})

 

  var sliderFases = tns({
    container: '.carousel-fases',
    items: 1,
    slideBy: 'page',
    autoplay: false,
    controls: true,
    autoplayButtonOutput: false,
    loop: true,
    nav: false,
    controlsContainer: '.fases-nav'
    });

  var sliderClinica = tns({
    container: '.carousel-clinica',
    items: 1,
    slideBy: 'page',
    gutter:10,
    autoplay: false,
    controls: true,
    autoplayButtonOutput: false,
    loop: false,
    nav: true,
    controlsContainer: '.clinica-nav'
    });

  // var sliderPasso = tns({
  //   container: '.passo-carrousel',
  //   items: 1,
  //   slideBy: 'page',
  //   autoplay: false,
  //   controls: true,
  //   autoplayButtonOutput: false,
  //   nav: false,
  //   loop: false,
  //   controlsContainer: '.passo-nav',
  //   responsive : {
  //     300: {
  //       disable: true,
  //     },
  //     1100 : {
  //       disable: false,
  //     },
  //   }
  // });



  var sliderDepo = tns({
    container: '.depo-container',
    items: 1,
    slideBy: 'page',
    autoplay: false,
    controls: true,
    autoplayButtonOutput: false,
    loop: true,
    nav: true,
    controlsContainer: '.depo-nav'
    });

    $('.next-button').on('click', function(e) {
      e.preventDefault();
      sliderDepo.goTo('next');
  })

  
$('#accordion button').on('click', function() {
  
  $('#accordion button i').removeClass('fa-chevron-up');
  $('#accordion button i').addClass('fa-chevron-down');
  

  var isOpen = $(this).attr('aria-expanded');
  let element = this.childNodes[2];

  if(isOpen === 'false') {
    
    element.classList.remove('fa-chevron-down');
    element.classList.add('fa-chevron-up');

  }
})


function validateEmail(emailField) {
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if (reg.test(emailField.value) == false) {
    alert("Email Inválido");
    return false;
  }

  return true;
}

$('.e-mail').attr('onChange',  "validateEmail(this)")


$(".phone").inputmask({
  placeholder: "",
  mask: function () {
    return ["(99) 9999-9999", "(99) 99999-9999"];
  },
});

$( document ).ready(function() {
  $('input[type="submit"').attr('onclick', ' gtag_report_conversion(url)');
});

$("#toplinks").on('click','a', function(event){ 
  event.preventDefault();
  var o =  $( $(this).attr("href") ).offset();   
  var sT = o.top - 85; 
  window.scrollTo(0,sT);
});

