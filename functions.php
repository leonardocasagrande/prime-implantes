<?php


function my_function_admin_bar(){
return false;
}
add_filter( 'show_admin_bar' , 'my_function_admin_bar');


function cptui_register_my_cpts_depoimento() {

	/**
	 * Post Type: Depoimentos.
	 */

	$labels = [
		"name" => __( "Depoimentos", "custom-post-type-ui" ),
		"singular_name" => __( "Depoimento", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Depoimentos", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "depoimento", "with_front" => true ],
		"query_var" => true,
	];

	register_post_type( "depoimento", $args );
}

add_action( 'init', 'cptui_register_my_cpts_depoimento' );



?>