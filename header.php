<!DOCTYPE html><html lang="pt_BR"><head><!-- Global site tag (gtag.js) - Google Analytics --><script async src="https://www.googletagmanager.com/gtag/js?id=UA-84790702-1"></script><script>window.dataLayer = window.dataLayer || [];

    function gtag() {
      dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-84790702-1');</script><!-- Event snippet for Solicitou Contato Ortodontia conversion page
In your html page, add the snippet and call gtag_report_conversion when someone clicks on the chosen link or button. --><script>function gtag_report_conversion(url) {
      var callback = function() {
        if (typeof(url) != 'undefined') {
          window.location = url;
        }
      };
      gtag('event', 'conversion', {
        'send_to': 'AW-1008107843/U8zWCNzk3KgBEMOC2uAD',
        'event_callback': callback
      });
      return false;
    }</script><meta charset="UTF-8"><meta name="viewport" content="width=device-width,initial-scale=1"><title>Prime Odonto - Implantes</title><meta name="robots" content="index, follow"><meta name="msapplication-TileColor" content="#ffffff"><meta name="theme-color" content="#ffffff"> <?php wp_head(); ?> <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet"><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css"><link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css"></head><body><div class="invisivel d-lg-none"></div><header class="d-lg-block d-none"><div class="container"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/logo.png" alt=""><div class="links"><a href="#tratamentos">Tratamentos</a> <a href="#problemas">Problemas</a> <a href="#passo">Passo a passo</a> <a href="#clinica">A clínica</a> <a href="#aparelhos">Aparelhos</a> <a href="#depo">Depoimentos</a> <a href="#contato">Contato</a></div></div></header><nav class="top-nav d-lg-none container" id="top-nav"><div class="logo-nav"></div><input class="menu-btn" type="checkbox" id="menu-btn"> <label class="menu-icon" for="menu-btn"><span class="navicon"></span></label><div id="toplinks" class="menu"><a href="#tratamentos">Tratamentos</a> <a href="#problemas">Problemas</a> <a href="#passo">Passo a passo</a> <a href="#clinica">A clínica</a> <a href="#aparelhos">Aparelhos</a> <a href="#depo">Depoimentos</a> <a href="#contato">Contato</a><div class="line-detail"></div><div class="midias"><a target="_blank" href="https://www.instagram.com/primeodontorj/"><i class="fab fa-instagram"></i> </a><a target="_blank" href="https://www.facebook.com/primeodontorj"><i class="fab fa-facebook-f"></i></a></div></div></nav></body></html>